package com.vuios.rxbus

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import io.reactivex.disposables.Disposable
import timber.log.Timber

class MainActivity : AppCompatActivity() {

    private lateinit var disposable: Disposable

    private lateinit var addressDisposable: Disposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val tvName = findViewById<TextView>(R.id.tvName)
        val tvAddress = findViewById<TextView>(R.id.tvAddress)

        val pager = findViewById<ViewPager>(R.id.view_pager)
        val adapter = SectionsPagerAdapter(supportFragmentManager)
        pager.adapter = adapter
        val tabs = findViewById<TabLayout>(R.id.tabs)
        tabs.setupWithViewPager(pager)

        disposable = RxBus.listen(RxEvent.EventAddress::class.java).subscribe {
            Timber.d("EventAddress %s", it.address)
            tvAddress.text = it.address
        }

        addressDisposable = RxBus.listen(RxEvent.EventName::class.java).subscribe {
            Timber.d("EventName %s", it.name)
            tvName.text = it.name
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!disposable.isDisposed) disposable.dispose()
        if (!addressDisposable.isDisposed) addressDisposable.dispose()
    }
}
