package com.vuios.rxbus

class RxEvent {
    data class EventName(val name: String)
    data class EventAddress(val address: String)
}