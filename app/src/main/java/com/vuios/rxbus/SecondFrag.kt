package com.vuios.rxbus

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import io.reactivex.disposables.Disposable
import timber.log.Timber

class SecondFrag : Fragment() {
    companion object {
        fun newInstance(): SecondFrag {
            return SecondFrag()
        }
    }

    private lateinit var disposable: Disposable

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_second, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val txtName = view.findViewById<TextView>(R.id.textViewName)

        // listen event here
        disposable = RxBus.listen(RxEvent.EventName::class.java).subscribe {
            Timber.d("EventName %s", it.name)
            txtName.text = it.name
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!disposable.isDisposed) disposable.dispose()
    }
}